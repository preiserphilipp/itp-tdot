<?php

class Settings {

    public static function getDirectory($dir) {
        if ($dir == '')
            return $_SERVER['DOCUMENT_ROOT'] . "/";

        return $_SERVER['DOCUMENT_ROOT'] . "/portal/" . $dir . "/";
    }

    public static function getPluginDir($plugin) {
        return Settings::getDirectory('plugin/' . $plugin);
    }

    public static function getStaticDirectory($dir) {
        return Settings::getDirectory('static') . $dir;
    }

    public static function getUrl($url) {
        return "/portal/" . $url;
    }

    public static function getFullUrl($url) {
        return "http://localhost" . Settings::getUrl($url);
    }

    public static function getStaticFullUrl($url) {
        return "http://localhost/portal/static/" . $url;
    }

    public static function getUserLogFile() {
        return Settings::getDirectory('logs') . 'user-log.txt';
    }

    public static function getErrorLogFile() {
        return Settings::getDirectory('logs') . 'error-log.txt';
    }

    public static function getPDO() {
        try {
            $pdo = new PDO('mysql:dbname=' . $DBNAME . ';charset=utf8;host=' . $DBHOST, $DBUSER, $DBPASSWORD);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            return $pdo;
        } catch (Exception $e) {
            Log::logError(Log::DBERROR, 'Settings', 'getPDO', $e->getMessage());
        }

        return null;
    }
}