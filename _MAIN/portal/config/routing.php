<?php
if (isset($_GET['modul'])) {
    if (in_array($_GET['modul'], $PLUGINS) && file_exists(Settings::getPluginDir($_GET['modul']) . 'routing.php'))
        include(Settings::getPluginDir($_GET['modul']) . 'routing.php');
    else
        $PAGE = Settings::getDirectory('error') . 'error404.php';
}
if ($PAGE == '')
    $PAGE = 'pages/default.php'; //DEFAULT PAGE