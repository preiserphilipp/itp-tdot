<?php

class Error {

    public static function DisplayError($code, $plugin, $message) {
        return '<h1>Error ' . $code . ' - File ' . $message . ' of plugin ' . $plugin . ' not found!</h1>';
    }
}