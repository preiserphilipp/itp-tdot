<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal</title>
    <?php
    if ($CSS != null)
        foreach ($CSS as $elem) {
            if ($elem != '')
                if (isset($_GET['modul']))
                    echo '<script type="text/javascript" src="' . Settings::getPluginDir($_GET['modul']) . 'css/' . $elem . '"></script>' . PHP_EOL;
                else
                    echo '<link rel="stylesheet" type="text/css" href="' . Settings::getStaticFullUrl('css/' . $elem) . '">' . PHP_EOL;
        }
    ?>
</head>
<body>
<div id="container">
            <?php
            if ($PAGE != null && $PAGE != "")
                if (isset($_GET['modul']) && in_array($_GET['modul'], $PLUGINS))
                    if (file_exists(Settings::getPluginDir($_GET['modul']) . 'pages/' . $PAGE))
                        include(Settings::getPluginDir($_GET['modul']) . 'pages/' . $PAGE);
                    else
                        echo Error::DisplayError(404, $_GET['modul'], $PAGE);
                else
                    include($PAGE);
            ?>
</div>
<?php
if ($JAVASCRIPT != null)
    foreach ($JAVASCRIPT as $elem) {
        if ($elem != '')
            if (isset($_GET['modul']))
                echo '<script type="text/javascript" src="' . Settings::getPluginDir($_GET['modul']) . 'js/' . $elem . '"></script>' . PHP_EOL;
            else
                echo '<script type="text/javascript" src="' . Settings::getStaticFullUrl('js/' . $elem) . '"></script>' . PHP_EOL;
    }
?>
</body>
</html>