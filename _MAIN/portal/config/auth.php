<?php

/*
 * Demoimplementierung zum Testen
 */

class Auth {
    private static $isTeacher = false;
    private static $isStudent = true;
    private static $isAdmin = false;
    private static $isHeadmaster = false;
    private static $infoStudent = array(
        'id' => 2,
        'firstname' => 'Max',
        'lastname' => 'Mustermann',
        'email' => 'max@mustermann.com',
        'matrNr' => 'S20100000', //MatrikelNummer
        'gender' => 'm', // m/w
        'klasse' => array(
            'department' => 'IT Zwettl',
            'headOfDepartment' => 'Anton Hauleitner',
            'year' => 2010, //Jahrgang
            'label' => '5BHIT', //Klassenbezeichnung
            'kspeaker' => array('firstname' => 'Master', 'lastname' => 'Speaker', 'email' => 'master@speaker.com'), //Klassensprecher
            'kspeakerStv' => array('firstname' => 'Slave', 'lastname' => 'Speaker', 'email' => 'slave@speaker.com'), //Klassensprecher-Stv.
            'jvorstand' => array('firstname' => 'Good', 'lastname' => 'Teacher', 'email' => 'good@teacher.at') //Jahrgangsvorstand
            ));
    private static $infoTeacher = array(
        'id' => 3,
        'firstname' => 'Good',
        'lastname' => 'Teacher',
        'email' => 'good@teacher.at',
        'short' => 'GOTE', //Lehrerkürzel
        'gender' => 'm', // m/w
        'department' => array('IT Zwettl', 'IT Krems')
    );

    public static function isTeacher() {
        return self::$isTeacher;
    }

    public static function isStudent() {
        return self::$isStudent;
    }

    public static function isAdmin() {
        return self::$isAdmin;
    }

    public static function isHeadmaster() {
        return self::$isHeadmaster;
    }

    public static function getInformations() {
        return self::$infoStudent; //change this to test student or teacher
    }
}