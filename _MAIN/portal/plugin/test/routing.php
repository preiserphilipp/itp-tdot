<?php

//MENÜAUFBAU -> key: name, der im menü angezeigt wird;
//              value: id auf die geroutet wird
$MENU = array("Menuelement" => "start");

/*
 * $TEMPLATE = true; // template anzeigen oder nicht (wenn das Template 'false' ist, dann wird nur der Inhalt des Plugins angezeigt)
 * $JAVASCRIPT = array('hallo.js'); // Einbinden von eigenem Javascript - Die JS Datei muss in das Verzeichnis 'static/js' kopiert werden
 * $CSS = array('css.css'); // Einbinden von eigenem CSS - Die CSS Datei muss in das Verzeichnis 'static/css' kopiert werden
 * $PAGE = 'start.php'; // definiert die Datei, die vom Portal eingebunden werden soll (die Dateien liegen im Verzeichnis 'pages' im plugin verzeichnis
 * $PAGETITLE = 'Test'; // Seitentitel
 */

if (isset($_GET['id'])) {
    switch (strtolower($_GET['id'])) {
        case 'add':
            $PAGE = 'add.php';
            break;
        default:
            $TEMPLATE = true;
            $JAVASCRIPT = array('hallo.js');
            $CSS = array('css.css');
            $PAGE = 'start.php';
            $PAGETITLE = 'Test';
            break;
    }
}
if ($PAGE == '') //default page - wird angezeigt, wenn keine ID übergeben wird
    $PAGE = 'start.php';