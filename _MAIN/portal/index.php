<?php
session_start();

date_default_timezone_set('Europe/Vienna');

$TEMPLATE = true;
$CSS = '';
$JAVASCRIPT = '';
$PAGETITLE = '';
$PAGE = '';

require_once('config/config.php');
require_once('config/settings.php');
require_once('config/routing.php');
require_once('config/log.php');
require_once('config/error.php');
require_once('config/auth.php');

if ($TEMPLATE)
    include('config/template.php');
else if ($PAGE != null && $PAGE != "")
    if (isset($_GET['modul']) && in_array($_GET['modul'], $PLUGINS))
        include(Settings::getPluginDir($_GET['modul']) . 'pages/' . $PAGE);
    else
        echo Error::DisplayError(404, $_GET['modul'], $PAGE);